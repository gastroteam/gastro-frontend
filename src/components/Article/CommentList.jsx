import React from "react";
import Comment from "./Comment";

function CommentList(props) {
  const { comments, currentUser, slug } = props;
  return (
    <div>
      {comments.map((comment) => {
        return (
          <Comment
            comment={comment}
            currentUser={currentUser}
            slug={slug}
            key={comment.id}
          />
        );
      })}
    </div>
  );
}

export default CommentList;
