import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router";
import ListErrors from "./ListErrors";
import agent from "../agent";
import {
  ADD_TAG,
  EDITOR_PAGE_LOADED,
  REMOVE_TAG,
  ARTICLE_SUBMITTED,
  EDITOR_PAGE_UNLOADED,
  UPDATE_FIELD_EDITOR,
} from "../constants/actionTypes";

function Editor({ errors, inProgress }) {
  const { slug } = useParams();

  const { articleSlug, body, description, tagInput, tagList, title } =
    useSelector((state) => state.editor);

  const [form, setForm] = useState({ body, description, tagInput, title });

  const dispatch = useDispatch();

  useEffect(() => {
    const unload = () => dispatch({ type: EDITOR_PAGE_UNLOADED });
    const payload = slug ? agent.Articles.get(slug) : null;

    if (slug) {
      unload();
    }

    dispatch({
      type: EDITOR_PAGE_LOADED,
      payload,
    });

    return unload;
  }, [slug]);

  const onChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
    dispatch({
      type: UPDATE_FIELD_EDITOR,
      key: e.target.name,
      value: e.target.value,
    });
  };

  const watchForEnter = (ev) => {
    if (ev.key === "Enter") {
      ev.preventDefault();
      dispatch({ type: ADD_TAG });
    }
  };

  const removeTagHandler = (tag) => () => {
    dispatch({ type: REMOVE_TAG, tag });
  };

  const submitForm = (ev) => {
    ev.preventDefault();
    const article = {
      title: form.title,
      description: form.description,
      body: form.body,
      tagList,
    };
    const slugToAssign = { slug: articleSlug };
    const promise = articleSlug
      ? agent.Articles.update(Object.assign(article, slugToAssign))
      : agent.Articles.create(article);

    dispatch({ type: ARTICLE_SUBMITTED, payload: promise });
  };

  return (
    <div className="editor-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-10 offset-md-1 col-xs-12">
            <ListErrors errors={errors} />

            <form>
              <fieldset>
                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="text"
                    name="title"
                    placeholder="Article Title"
                    value={title}
                    onChange={onChange}
                  />
                </fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control"
                    type="text"
                    name="description"
                    placeholder="What's this article about?"
                    value={description}
                    onChange={onChange}
                  />
                </fieldset>

                <fieldset className="form-group">
                  <textarea
                    className="form-control"
                    rows="8"
                    name="body"
                    placeholder="Write your article (in markdown)"
                    value={body}
                    onChange={onChange}
                  />
                </fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control"
                    type="text"
                    name="tagInput"
                    placeholder="Enter tags"
                    value={tagInput}
                    onChange={onChange}
                    onKeyUp={watchForEnter}
                  />

                  <div className="tag-list">
                    {(tagList || []).map((tag) => {
                      return (
                        <span className="tag-default tag-pill" key={tag}>
                          <i
                            className="ion-close-round"
                            onClick={removeTagHandler(tag)}
                          />
                          {tag}
                        </span>
                      );
                    })}
                  </div>
                </fieldset>

                <button
                  className="btn btn-lg pull-xs-right btn-primary"
                  type="button"
                  disabled={inProgress}
                  onClick={submitForm}
                >
                  Publish Article
                </button>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Editor;
