import { Link } from "react-router-dom";

export default function EditProfileSettings({ isUser }) {
  return isUser ? (
    <Link
      to="/settings"
      className="btn btn-sm btn-outline-secondary action-btn"
    >
      <i className="ion-gear-a" />
      Edit Profile Settings
    </Link>
  ) : null;
}
