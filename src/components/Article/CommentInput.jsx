import { useState } from "react";
import { useDispatch } from "react-redux";

import { ADD_COMMENT } from "../../constants/actionTypes";
import agent from "../../agent";

function CommentInput({ slug, currentUser }) {
  const [body, setBody] = useState("");

  const dispatch = useDispatch();

  const createComment = (e) => {
    e.preventDefault();
    dispatch({
      type: ADD_COMMENT,
      payload: agent.Comments.create(slug, { body }),
    });
  };

  return (
    <form className="card comment-form" onSubmit={createComment}>
      <div className="card-block">
        <textarea
          className="form-control"
          placeholder="Write a comment..."
          value={body}
          onChange={(e) => setBody(e.target.value)}
          rows="3"
        />
      </div>
      <div className="card-footer">
        <img
          src={currentUser.image}
          className="comment-author-img"
          alt={currentUser.username}
        />
        <button className="btn btn-sm btn-primary" type="submit">
          Post Comment
        </button>
      </div>
    </form>
  );
}

export default CommentInput;
