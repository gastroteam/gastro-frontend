import { Link } from "react-router-dom";
import { React, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import ListErrors from "./ListErrors";
import agent from "../agent";
import {
  UPDATE_FIELD_AUTH,
  LOGIN,
  LOGIN_PAGE_UNLOADED,
} from "../constants/actionTypes";

export default function Login() {
  const { errors, inProgress } = useSelector((state) => state.auth);

  const dispatch = useDispatch();

  useEffect(() => {
    return function onUnload() {
      dispatch({ type: LOGIN_PAGE_UNLOADED });
    };
  }, []);

  const [form, setValue] = useState({ email: "", password: "" });

  const handleFormChange = (e) => {
    setValue({ ...form, [e.target.name]: e.target.value });
  };

  const submitForm = (ev) => {
    ev.preventDefault();
    dispatch({
      type: UPDATE_FIELD_AUTH,
      key: "email",
      value: form.email,
    });
    dispatch({
      type: UPDATE_FIELD_AUTH,
      key: "password",
      value: form.password,
    });
    dispatch({
      type: LOGIN,
      payload: agent.Auth.login(form.email, form.password),
    });
  };

  return (
    <div className="auth-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-6 offset-md-3 col-xs-12">
            <h1 className="text-xs-center">Sign In</h1>
            <p className="text-xs-center">
              <Link to="/register">Need an account?</Link>
            </p>

            <ListErrors errors={errors} />

            <form onSubmit={submitForm}>
              <fieldset>
                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="email"
                    name="email"
                    placeholder="Email"
                    value={form.email}
                    onChange={handleFormChange}
                  />
                </fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="password"
                    name="password"
                    placeholder="Password"
                    value={form.password}
                    onChange={handleFormChange}
                  />
                </fieldset>

                <button
                  className="btn btn-lg btn-primary pull-xs-right"
                  type="submit"
                  disabled={inProgress}
                >
                  Sign in
                </button>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
