import React from "react";

function TagFilterTab(props) {
  const { tag } = props;
  if (!tag) {
    return null;
  }

  return (
    <li className="nav-item">
      <a href="" className="nav-link active">
        <i className="ion-pound" /> {tag}
      </a>
    </li>
  );
}

export default TagFilterTab;
