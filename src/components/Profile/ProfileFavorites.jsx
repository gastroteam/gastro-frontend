import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import agent from "../../agent";
import ProfileRender from "./ProfileRender";
import {
  PROFILE_PAGE_LOADED,
  PROFILE_PAGE_UNLOADED,
} from "../../constants/actionTypes";

export default function ProfileFavorites() {
  const dispatch = useDispatch();
  const { username } = useParams();

  useEffect(() => {
    dispatch({
      type: PROFILE_PAGE_LOADED,
      pager: (page) => agent.Articles.favoritedBy(username, page),
      payload: Promise.all([
        agent.Profile.get(username),
        agent.Articles.favoritedBy(username),
      ]),
    });
    return () => dispatch({ type: PROFILE_PAGE_UNLOADED });
  }, []);

  return <ProfileRender favorites />;
}
