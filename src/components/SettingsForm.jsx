import { useState } from "react";
import { useSelector } from "react-redux";

function SettingsForm({ currentUser, onSubmitForm }) {
  const inProgress = useSelector((store) => store.common.inProgress);

  const [form, updateForm] = useState({
    image: currentUser ? currentUser.image : "",
    username: currentUser ? currentUser.username : "",
    bio: currentUser ? currentUser.bio : "",
    email: currentUser ? currentUser.email : "",
    password: currentUser ? currentUser.password : "",
  });

  const updateState = (e) => {
    updateForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <form
      onSubmit={(e) => {
        e.prevent.default();
        onSubmitForm(form);
      }}
    >
      <fieldset>
        <fieldset className="form-group">
          <input
            className="form-control"
            type="text"
            placeholder="URL of profile picture"
            name="image"
            value={form.image}
            onChange={updateState}
          />
        </fieldset>

        <fieldset className="form-group">
          <input
            className="form-control form-control-lg"
            type="text"
            name="username"
            placeholder="Username"
            value={form.username}
            onChange={updateState}
          />
        </fieldset>

        <fieldset className="form-group">
          <textarea
            className="form-control form-control-lg"
            rows="8"
            placeholder="Short bio about you"
            name="bio"
            value={form.bio}
            onChange={updateState}
          />
        </fieldset>

        <fieldset className="form-group">
          <input
            className="form-control form-control-lg"
            type="email"
            placeholder="Email"
            name="email"
            value={form.email}
            onChange={updateState}
          />
        </fieldset>

        <fieldset className="form-group">
          <input
            className="form-control form-control-lg"
            type="password"
            name="password"
            placeholder="New Password"
            value={form.password}
            onChange={updateState}
          />
        </fieldset>

        <button
          className="btn btn-lg btn-primary pull-xs-right"
          type="submit"
          disabled={inProgress}
        >
          Update Settings
        </button>
      </fieldset>
    </form>
  );
}

export default SettingsForm;
