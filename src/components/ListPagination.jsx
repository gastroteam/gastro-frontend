import React from "react";
import { connect } from "react-redux";
import agent from "../agent";
import { SET_PAGE } from "../constants/actionTypes";

const mapDispatchToProps = (dispatch) => ({
  onSetPage: (page, payload) => dispatch({ type: SET_PAGE, page, payload }),
});

function ListPagination(props) {
  const { articlesCount } = props;

  if (articlesCount <= 10) {
    return null;
  }

  const range = [];
  for (let i = 0; i < Math.ceil(articlesCount / 10); i += 1) {
    range.push(i);
  }

  const setPage = (page) => {
    const { pager } = props;
    if (pager) {
      props.onSetPage(page, pager(page));
    } else {
      props.onSetPage(page, agent.Articles.all(page));
    }
  };

  const { currentPage } = props;

  return (
    <nav>
      <ul className="pagination">
        {range.map((v) => {
          const isCurrent = v === currentPage;
          const onClick = (ev) => {
            ev.preventDefault();
            setPage(v);
          };
          return (
            <li
              className={isCurrent ? "page-item active" : "page-item"}
              onClick={onClick}
              key={v.toString()}
            >
              <a className="page-link" href="">
                {v + 1}
              </a>
            </li>
          );
        })}
      </ul>
    </nav>
  );
}

export default connect(() => ({}), mapDispatchToProps)(ListPagination);
