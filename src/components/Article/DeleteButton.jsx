import React from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import agent from "../../agent";
import { DELETE_COMMENT } from "../../constants/actionTypes";

function DeleteButton({ show, commentId }) {
  const dispatch = useDispatch();
  const { slug } = useParams();

  const del = () => {
    const payload = agent.Comments.delete(slug, commentId);
    dispatch({ type: DELETE_COMMENT, payload, commentId });
  };

  if (!show) return null;

  return (
    <span className="mod-options">
      <i className="ion-trash-a" onClick={del} />
    </span>
  );
}

export default DeleteButton;
