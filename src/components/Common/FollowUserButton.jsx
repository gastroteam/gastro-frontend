export default function FollowUserButton({ isUser, user, unfollow, follow }) {
  let classes = "btn btn-sm action-btn";
  if (user.following) {
    classes += " btn-secondary";
  } else {
    classes += " btn-outline-secondary";
  }

  const handleClick = (ev) => {
    ev.preventDefault();
    if (user.following) {
      unfollow(user.username);
    } else {
      follow(user.username);
    }
  };

  return isUser ? (
    <button type="button" className={classes} onClick={handleClick}>
      <i className="ion-plus-round" />
      &nbsp;
      {user.following ? "Unfollow" : "Follow"} {user.username}
    </button>
  ) : null;
}
