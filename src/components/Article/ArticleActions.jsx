import { Link } from "react-router-dom";
import React from "react";
import { useDispatch } from "react-redux";
import agent from "../../agent";
import { DELETE_ARTICLE } from "../../constants/actionTypes";

function ArticleActions(props) {
  const { article, canModify } = props;

  const dispatch = useDispatch();

  const onClickDelete = (payload) => {
    dispatch({ type: DELETE_ARTICLE, payload });
  };

  const del = () => {
    onClickDelete(agent.Articles.del(article.slug));
  };
  if (canModify) {
    return (
      <span>
        <Link
          to={`/editor/${article.slug}`}
          className="btn btn-outline-secondary btn-sm"
        >
          <i className="ion-edit" /> Edit Article
        </Link>

        <button
          type="button"
          className="btn btn-outline-danger btn-sm"
          onClick={del}
        >
          <i className="ion-trash-a" /> Delete Article
        </button>
      </span>
    );
  }

  return <span />;
}

export default ArticleActions;
