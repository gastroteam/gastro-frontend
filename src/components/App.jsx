import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { Outlet } from "react-router-dom";
import { push } from "redux-first-history";
import Header from "./Header";
import { APP_LOAD, REDIRECT } from "../constants/actionTypes";
import agent from "../agent";

function App() {
  const dispatch = useDispatch();

  const { appName, currentUser, appLoaded, redirectTo } = useSelector(
    (store) => store.common,
  );

  useEffect(() => {
    const token = window.localStorage.getItem("jwt");

    if (token) {
      agent.setToken(token);
    }

    dispatch({
      type: APP_LOAD,
      payload: token ? agent.Auth.current() : null,
      token,
      skipTracking: true,
    });
  }, []);

  useEffect(() => {
    if (redirectTo) {
      dispatch(push(redirectTo));
      dispatch({ type: REDIRECT });
    }
  }, [redirectTo, dispatch]);

  return (
    <div>
      <Header appName={appName} currentUser={currentUser} />
      {appLoaded && <Outlet />}
    </div>
  );
}

export default App;
