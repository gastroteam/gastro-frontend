import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";
import React from "react";
import { Route, Routes } from "react-router-dom";
import { HistoryRouter as Router } from "redux-first-history/rr6";
import { store, history } from "./store";
import Article from "./components/Article";
import Editor from "./components/Editor";
import Home from "./components/Home";
import Login from "./components/Login";
import Profile from "./components/Profile/Profile";
import ProfileFavorites from "./components/Profile/ProfileFavorites";
import Register from "./components/Register";
import Settings from "./components/Settings";
import App from "./components/App";

const root = createRoot(document.getElementById("root"));

root.render(
  <Provider store={store}>
    <Router history={history}>
      <Routes>
        <Route path="/" element={<App />}>
          <Route index element={<Home />} />
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
          <Route path="editor/:slug" element={<Editor />} />
          <Route path="editor" element={<Editor />} />
          <Route path="article/:id" element={<Article />} />
          <Route path="settings" element={<Settings />} />
          <Route
            path="profile/:username/favorites"
            element={<ProfileFavorites />}
          />
          <Route path="profile/:username" element={<Profile />} />
        </Route>
      </Routes>
    </Router>
  </Provider>,
);
