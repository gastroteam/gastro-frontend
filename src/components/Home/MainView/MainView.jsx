import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { CHANGE_TAB } from "../../../constants/actionTypes";
import ArticleList from "../../ArticleList";
import GlobalFeedTab from "./GlobalFeedTab";
import TagFilterTab from "./TagFilterTab";
import YourFeedTab from "./YourFeedTab";

function MainView() {
  const dispatch = useDispatch();

  const onTabClick = (tab, pager, payload) =>
    dispatch({ type: CHANGE_TAB, tab, pager, payload });

  const token = useSelector((state) => state.common);
  const tag = useSelector((state) => state.home.tag);
  const { pager, loading, articles, articlesCount, currentPage, tab } =
    useSelector((state) => state.articleList);

  return (
    <div className="col-md-9">
      <div className="feed-toggle">
        <ul className="nav nav-pills outline-active">
          <YourFeedTab token={token} tab={tab} onTabClick={onTabClick} />

          <GlobalFeedTab tab={tab} onTabClick={onTabClick} />

          <TagFilterTab tag={tag} />
        </ul>
      </div>

      <ArticleList
        pager={pager}
        articles={articles}
        loading={loading}
        articlesCount={articlesCount}
        currentPage={currentPage}
      />
    </div>
  );
}

export default MainView;
