import { applyMiddleware, combineReducers, createStore } from "redux";
import { createLogger } from "redux-logger";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import { createReduxHistoryContext } from "redux-first-history";
import { createBrowserHistory } from "history";
import { promiseMiddleware, localStorageMiddleware } from "./middleware";

import article from "./reducers/article";
import articleList from "./reducers/articleList";
import auth from "./reducers/auth";
import common from "./reducers/common";
import editor from "./reducers/editor";
import home from "./reducers/home";
import profile from "./reducers/profile";
import settings from "./reducers/settings";

const { routerReducer, createReduxHistory, routerMiddleware } =
  createReduxHistoryContext({
    history: createBrowserHistory(),
  });

const getMiddleware = () => {
  if (process.env.NODE_ENV === "production") {
    return applyMiddleware(
      routerMiddleware,
      promiseMiddleware,
      localStorageMiddleware,
    );
  }
  return applyMiddleware(
    routerMiddleware,
    promiseMiddleware,
    localStorageMiddleware,
    createLogger(),
  );
};

const reducer = combineReducers({
  article,
  articleList,
  auth,
  common,
  editor,
  home,
  profile,
  settings,
  router: routerReducer,
});

export const store = createStore(reducer, composeWithDevTools(getMiddleware()));

export const history = createReduxHistory(store);
