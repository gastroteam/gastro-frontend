import { useDispatch } from "react-redux";
import ListErrors from "./ListErrors";

import {
  SETTINGS_SAVED,
  SETTINGS_PAGE_UNLOADED,
  LOGOUT,
} from "../constants/actionTypes";
import SettingsForm from "./SettingsForm";
import agent from "../agent";

function Settings({ errors, currentUser }) {
  const dispatch = useDispatch();

  const onClickLogout = () => {
    dispatch({ type: LOGOUT });
  };

  const onSubmitForm = (user) => {
    dispatch({ type: SETTINGS_SAVED, payload: agent.Auth.save(user) });
    dispatch({ type: SETTINGS_PAGE_UNLOADED });
  };

  return (
    <div className="settings-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-6 offset-md-3 col-xs-12">
            <h1 className="text-xs-center">Your Settings</h1>

            <ListErrors errors={errors} />

            <SettingsForm
              currentUser={currentUser}
              onSubmitForm={onSubmitForm}
            />

            <hr />

            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={onClickLogout}
            >
              Or click here to logout.
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Settings;
