import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import agent from "../../agent";
import {
  PROFILE_PAGE_LOADED,
  PROFILE_PAGE_UNLOADED,
} from "../../constants/actionTypes";
import ProfileRender from "./ProfileRender";

export default function Profile() {
  const dispatch = useDispatch();
  const { username } = useParams();

  useEffect(() => {
    dispatch({
      type: PROFILE_PAGE_LOADED,
      payload: Promise.all([
        agent.Profile.get(username),
        agent.Articles.byAuthor(username),
      ]),
    });
    return () => dispatch({ type: PROFILE_PAGE_UNLOADED });
  }, []);

  return <ProfileRender />;
}
