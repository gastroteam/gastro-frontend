import { Link, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import EditProfileSettings from "../Common/EditProfileSettings";
import FollowUserButton from "../Common/FollowUserButton";
import ArticleList from "../ArticleList";
import { FOLLOW_USER, UNFOLLOW_USER } from "../../constants/actionTypes";
import agent from "../../agent";

export default function ProfileRender({ favorites }) {
  const { username } = useParams();
  const { profile, currentUser, pager, articles, articlesCount, currentPage } =
    useSelector((state) => ({
      profile: state.profile,
      currentUser: state.common.currentUser,
      pager: state.articleList.pager,
      articles: state.articleList.articles,
      currentPage: state.articleList.currentPage,
      articlesCount: state.articleList.articlesCount,
    }));
  const dispatch = useDispatch();

  const onFollow = (user) => {
    dispatch({
      type: FOLLOW_USER,
      payload: agent.Profile.follow(user),
    });
  };

  const onUnfollow = (user) => {
    dispatch({
      type: UNFOLLOW_USER,
      payload: agent.Profile.unfollow(user),
    });
  };

  const isUser = currentUser && profile.username === currentUser.username;

  if (!profile) {
    return;
  }

  return (
    <div className="profile-page">
      <div className="user-info">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-md-10 offset-md-1">
              <img
                src={profile.image}
                className="user-img"
                alt={profile.username}
              />
              <h4>{profile.username}</h4>
              <p>{profile.bio}</p>

              <EditProfileSettings isUser={isUser} />
              <FollowUserButton
                isUser={isUser}
                user={profile}
                follow={onFollow}
                unfollow={onUnfollow}
              />
            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-xs-12 col-md-10 offset-md-1">
            <div className="articles-toggle">
              <ul className="nav nav-pills outline-active">
                <li className="nav-item">
                  <Link
                    className={`nav-link ${favorites ? "" : "active"}`}
                    to={`/profile/${username}`}
                  >
                    My Articles
                  </Link>
                </li>

                <li className="nav-item">
                  <Link
                    className={`nav-link ${favorites ? "active" : ""}`}
                    to={`/profile/${username}/favorites`}
                  >
                    Favorited Articles
                  </Link>
                </li>
              </ul>
            </div>
            <ArticleList
              pager={pager}
              articles={articles}
              articlesCount={articlesCount}
              state={currentPage}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
