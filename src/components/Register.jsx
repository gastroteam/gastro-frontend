import { Link } from "react-router-dom";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import ListErrors from "./ListErrors";
import agent from "../agent";
import {
  UPDATE_FIELD_AUTH,
  REGISTER,
  REGISTER_PAGE_UNLOADED,
} from "../constants/actionTypes";

export default function Register() {
  const dispatch = useDispatch();

  const { errors, username, email, password, inProgress } = useSelector(
    (store) => store.auth,
  );

  const handleChange = (ev) => {
    dispatch({
      type: UPDATE_FIELD_AUTH,
      key: ev.target.name,
      value: ev.target.value,
    });
  };

  const submitForm = (username, email, password) => (ev) => {
    ev.preventDefault();
    const payload = agent.Auth.register(username, email, password);
    dispatch({ type: REGISTER, payload });
  };

  useEffect(() => {
    return () => {
      dispatch({ type: REGISTER_PAGE_UNLOADED });
    };
  }, []);

  return (
    <div className="auth-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-6 offset-md-3 col-xs-12">
            <h1 className="text-xs-center">Sign Up</h1>
            <p className="text-xs-center">
              <Link to="/login">Have an account?</Link>
            </p>

            <ListErrors errors={errors} />

            <form onSubmit={submitForm(username, email, password)}>
              <fieldset>
                <fieldset className="form-group">
                  <input
                    name="username"
                    className="form-control form-control-lg"
                    type="text"
                    placeholder="Username"
                    value={username}
                    onChange={handleChange}
                  />
                </fieldset>

                <fieldset className="form-group">
                  <input
                    name="email"
                    className="form-control form-control-lg"
                    type="email"
                    placeholder="Email"
                    value={email}
                    onChange={handleChange}
                  />
                </fieldset>

                <fieldset className="form-group">
                  <input
                    name="password"
                    className="form-control form-control-lg"
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={handleChange}
                  />
                </fieldset>

                <button
                  className="btn btn-lg btn-primary pull-xs-right"
                  type="submit"
                  disabled={inProgress}
                >
                  Sign up
                </button>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
