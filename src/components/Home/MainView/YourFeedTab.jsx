import React from "react";
import agent from "../../../agent";

function YourFeedTab(props) {
  const { token, onTabClick, tab } = props;

  if (!token) {
    return null;
  }

  const clickHandler = (ev) => {
    ev.preventDefault();
    onTabClick("feed", agent.Articles.feed, agent.Articles.feed());
  };

  return (
    <li className="nav-item">
      <a
        href=""
        className={tab === "feed" ? "nav-link active" : "nav-link"}
        onClick={clickHandler}
      >
        Your Feed
      </a>
    </li>
  );
}

export default YourFeedTab;
